COPY square.t_privilege(lastchange, pk, name) FROM stdin;
2021-08-12 09:04:19.192387	377000	\N
2021-08-12 09:04:19.192387	377035	\N
2021-08-12 09:04:19.192387	377013	\N
2021-08-12 09:04:19.192387	377014	\N
2021-08-12 09:04:19.192387	376998	\N
2021-08-12 09:04:19.192387	376997	\N
2021-08-12 09:04:19.192387	377006	\N
2021-08-12 09:04:19.192387	377011	\N
2021-08-12 09:04:19.192387	377008	\N
2021-08-12 09:04:19.192387	377009	\N
2021-08-12 09:04:19.192387	377015	\N
2021-08-12 09:04:19.192387	377001	\N
2021-08-12 09:04:19.192387	377002	\N
2021-08-12 09:04:19.192387	377012	\N
2021-08-12 09:04:19.192387	377010	\N
2021-08-12 09:04:19.192387	377003	\N
2021-08-12 09:04:19.192387	377004	\N
2021-08-12 09:04:19.192387	377005	\N
2021-08-12 09:04:19.192387	377007	\N
\.

COPY square.t_function (lastchange, pk, name, before, body, after, test, activated, category, type, creator, creationdate, summary, description, varname, varlist, dataframe, vardef, fk_privilege, datasource, use_intervals, vignette) FROM stdin;
2021-08-12 09:04:15.848718	295	acc_margins	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::acc_margins)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::acc_margins))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Function to estimate marginal means, see\nemmeans::emmeans	margins does calculations for quality indicator\nUnexpected distribution wrt location (link). Therefore we pursue a combined\napproach of descriptive and model-based statistics to investigate differences\nacross the levels of an auxiliary variable.\nCAT: Unexpected distribution w.r.t. location\nMarginal means\nMarginal means rests on model based results, i.e. a significantly different\nmarginal mean depends on sample size. Particularly in large studies, small\nand irrelevant differences may become significant. The contrary holds if\nsample size is low.	resp_vars	f	study_data	meta_data	377000	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_acc_impl_margins.html
2021-08-12 09:04:18.754259	311	int_datatype_matrix		\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::int_datatype_matrix)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::int_datatype_matrix))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    			t	\N	\N	0	2021-06-04 17:28:33.751699	Function to check declared data types of metadata in study data	Checks data types of the study data and for the data type\ndeclared in the metadata	resp_vars	f	study_data	meta_data	377035	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_int_impl_datatype_matrix.html
2021-08-12 09:04:19.007093	308	export_rdata_list	\N	\n# created by functionregistrator (Version 0.1.7)\nsquare.save.env(file = "square.data.RData")\nf <- square.image.png2tex("square.data.RData")\nreturn(list(rdata = f))\n	\N	\N	f	\N	\N	0	2021-03-23 18:34:09.896893	Exporting RData list function	Export RData file for offline development (multivariate)	resp_vars	t	study_data	meta_data	377013	raw	t	https://dataquality.ship-med.uni-greifswald.de/
2021-08-12 09:04:19.192387	309	export_rdata_no_list	\N	\n# created by functionregistrator (Version 0.1.7)\nsquare.save.env(file = "square.data.RData")\nf <- square.image.png2tex("square.data.RData")\nreturn(list(rdata = f))\n	\N	\N	f	\N	\N	0	2021-03-23 18:34:09.896893	Exporting RData non-list function	Export RData file for offline development (univariate)	resp_vars	f	study_data	meta_data	377014	raw	t	https://dataquality.ship-med.uni-greifswald.de/
2021-08-12 09:04:15.401369	293	acc_end_digits		\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::acc_end_digits)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::acc_end_digits))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    			t	\N	\N	0	2021-03-23 18:34:09.896893	Extension of\nacc_shape_or_scale\nto examine uniform distributions of\nend digits	This implementation contrasts the empirical distribution of a measurement\nvariables against assumed distributions. The approach is adapted from the\nidea of rootograms (Tukey (1977)) which is also applicable for count data\n(Kleiber and Zeileis (2016)).	resp_vars	f	study_data	meta_data	376998	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_acc_impl_end_digits.html
2021-08-12 09:04:15.184134	292	acc_distributions	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::acc_distributions)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::acc_distributions))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Function to plot histograms added by empirical cumulative distributions\nfor subgroups	Function to identify inadmissible measurements according to hard limits\n(multiple variables)	resp_vars	f	study_data	meta_data	376997	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_acc_impl_distributions.html
2021-08-12 09:04:17.255634	301	com_item_missingness	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::com_item_missingness)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::com_item_missingness))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Summarize missingness columnwise (in variable)	Item-Missingness (also referred to as item nonresponse (De Leeuw et al.\n2003)) describes the missingness of single values, e.g. blanks or empty data\ncells in a data set. Item-Missingness occurs for example in case a respondent\ndoes not provide information for a certain question, a question is overlooked\nby accident, a programming failure occurs or a provided answer were missed\nwhile entering the data.	resp_vars	f	study_data	meta_data	377006	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_com_impl_item_missingness.html
2021-08-12 09:04:18.335542	306	con_inadmissible_categorical	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::con_inadmissible_categorical)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::con_inadmissible_categorical))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Detects variable levels not specified in metadata	For each categorical variable, value lists should be defined in the metadata.\nThis implementation will examine, if all observed levels in the study data\nare valid.	resp_vars	f	study_data	meta_data	377011	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_con_impl_inadmissible_categorical.html
2021-08-12 09:04:17.735558	303	com_unit_missingness	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::com_unit_missingness)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::com_unit_missingness))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Counts all individuals with no measurements at all	This implementation examines a crude version of unit missingness or\nunit-nonresponse (Kalton and Kasprzyk 1986), i.e. if all measurement\nvariables in the study data are missing for an observation it has unit\nmissingness.\nThe function can be applied on stratified data. In this case strata_vars must\nbe specified.	resp_vars	f	study_data	meta_data	377008	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_com_impl_unit_missingness.html
2021-08-12 09:04:17.917143	304	con_contradictions	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::con_contradictions)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::con_contradictions))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	f	\N	\N	0	2021-03-23 18:34:09.896893	Checks user-defined contradictions in study data	This approach considers a contradiction if impossible combinations of data\nare observed in one participant. For example, if age of a participant is\nrecorded repeatedly the value of age is (unfortunately) not able to decline.\nMost cases of contradictions rest on comparison of two variables.\nImportant to note, each value that is used for comparison may represent a\npossible characteristic but the combination of these two values is considered\nto be impossible. The approach does not consider implausible or inadmissible\nvalues.	resp_vars	f	study_data	meta_data	377009	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_con_impl_contradictions.html
2021-08-12 09:04:19.377953	310	export_rdata_aggregate	\N	\n# created by functionregistrator (Version 0.1.7)\nsquare.save.env(file = "square.data.RData")\nf <- square.image.png2tex("square.data.RData")\nreturn(list(rdata = f))\n	\N	\N	f	\N	\N	0	2021-03-23 18:34:09.896893	Exporting RData calcres function	Export RData file for offline development (calculation results)	resp_vars	\N	study_data	meta_data	377015	raw	t	https://dataquality.ship-med.uni-greifswald.de/
2021-08-12 09:04:16.11576	296	acc_multivariate_outlier	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::acc_multivariate_outlier)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::acc_multivariate_outlier))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Function to calculate and plot Mahalanobis distances	A standard tool to detect multivariate outliers is the Mahalanobis distance.\nThis approach is very helpful for the interpretation of the plausibility of a\nmeasurement given the value of another.\nIn this approach the Mahalanobis distance is used as a univariate measure\nitself. We apply the same rules for the identification of outliers as in\nunivariate outliers:\nthe classical approach from Tukey:\n1.5 * IQR\nfrom the\n1st (\nQ_{25}\n) or 3rd (\nQ_{75}\n) quartile.\nthe\n6* \\sigma\napproach, i.e. any measurement of the Mahalanobis\ndistance not in the interval of\n\\bar{x} \\pm 3*\\sigma\nis considered an\noutlier.\nthe approach from Hubert for skewed distributions which is embedded in the\nR package\nrobustbase\na completely heuristic approach named\n\\sigma\n-gap.\nFor further details, please see the vignette for univariate outlier.	resp_vars	t	study_data	meta_data	377001	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_acc_impl_multivariate_outlier.html
2021-08-12 09:04:16.326755	297	acc_robust_univariate_outlier	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::acc_robust_univariate_outlier)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::acc_robust_univariate_outlier))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	f	\N	\N	0	2021-03-23 18:34:09.896893	Function to identify univariate outliers by four different approaches	A classical but still popular approach to detect univariate outlier is the\nboxplot method introduced by Tukey 1977. The boxplot is a simple graphical\ntool to display information about continuous univariate data (e.g., median,\nlower and upper quartile). Outliers are defined as values deviating more\nthan\n1.5 \\times IQR\nfrom the 1st (Q25) or 3rd (Q75) quartile. The\nstrength of Tukey’s method is that it makes no distributional assumptions\nand thus is also applicable to skewed or non mound-shaped data\nMarsh and Seo, 2006. Nevertheless, this method tends to identify frequent\nmeasurements which are falsely interpreted as true outliers.\nA somewhat more conservative approach in terms of symmetric and/or normal\ndistributions is the\n6 * \\sigma\napproach, i.e. any measurement not in\nthe interval of\nmean(x) +/- 3 * \\sigma\nis considered an outlier.\nBoth methods mentioned above are not ideally suited to skewed distributions.\nAs many biomarkers such as laboratory measurements represent in skewed\ndistributions the methods above may be insufficient. The approach of Hubert\nand Vandervieren 2008 adjusts the boxplot for the skewness of the\ndistribution. This approach is implemented in several R packages such as\nrobustbase::mc\nwhich is used in this implementation of\ndataquieR\n.\nAnother completely heuristic approach is also included to identify outliers.\nThe approach is based on the assumption that the distances between\nmeasurements of the same underlying distribution should homogeneous. For\ncomprehension of this approach:\nconsider an ordered sequence of all measurements.\nbetween these measurements all distances are calculated.\nthe occurrence of larger distances between two neighboring measurements\nmay\nthan indicate a distortion of the data. For the heuristic definition of a\nlarge distance\n1 * \\sigma\nhas been been chosen.\nNote, that the plots are not deterministic, because they use\nggplot2::geom_jitter\n.	resp_vars	f	study_data	meta_data	377002	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_acc_impl_robust_univariate_outlier.html
2021-08-12 09:04:18.53477	307	con_limit_deviations	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::con_limit_deviations)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::con_limit_deviations))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Detects variable values exceeding limits defined in metadata	APPROACH\nInadmissible numerical values can be of type integer or float. This\nimplementation requires the definition of intervals in the metadata to\nexamine the admissibility of numerical study data.\nThis helps identify inadmissible measurements according to\nhard limits (for multiple variables).	resp_vars	f	study_data	meta_data	377012	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_con_impl_limit_deviations.html
2021-08-12 09:04:18.161439	305	con_detection_limits	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::con_detection_limits)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::con_detection_limits))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	f	\N	\N	0	2021-03-23 18:34:09.896893	con_detection_limits	APPROACH\nInadmissible numerical values can be of type integer or float. This\nimplementation requires the definition of intervals in the metadata to\nexamine the admissibility of numerical study data.\nThis helps identify inadmissible measurements according to\nhard limits (for multiple variables).	resp_vars	f	study_data	meta_data	377010	raw	t	https://dataquality.ship-med.uni-greifswald.de/
2021-08-12 09:04:16.536511	298	acc_shape_or_scale	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::acc_shape_or_scale)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::acc_shape_or_scale))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Function to compare observed versus expected distributions	This implementation contrasts the empirical distribution of a measurement\nvariables against assumed distributions. The approach is adapted from the\nidea of rootograms (Tukey 1977) which is also applicable for count data\n(Kleiber and Zeileis 2016).	resp_vars	f	study_data	meta_data	377003	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_acc_impl_shape_or_scale.html
2021-08-12 09:04:16.798491	299	acc_univariate_outlier	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::acc_univariate_outlier)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::acc_univariate_outlier))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Function to identify univariate outliers by four different approaches	A classical but still popular approach to detect univariate outlier is the\nboxplot method introduced by Tukey 1977. The boxplot is a simple graphical\ntool to display information about continuous univariate data (e.g., median,\nlower and upper quartile). Outliers are defined as values deviating more\nthan\n1.5 \\times IQR\nfrom the 1st (Q25) or 3rd (Q75) quartile. The\nstrength of Tukey’s method is that it makes no distributional assumptions\nand thus is also applicable to skewed or non mound-shaped data\nMarsh and Seo, 2006. Nevertheless, this method tends to identify frequent\nmeasurements which are falsely interpreted as true outliers.\nA somewhat more conservative approach in terms of symmetric and/or normal\ndistributions is the\n6 * \\sigma\napproach, i.e. any measurement not in\nthe interval of\nmean(x) +/- 3 * \\sigma\nis considered an outlier.\nBoth methods mentioned above are not ideally suited to skewed distributions.\nAs many biomarkers such as laboratory measurements represent in skewed\ndistributions the methods above may be insufficient. The approach of Hubert\nand Vandervieren 2008 adjusts the boxplot for the skewness of the\ndistribution. This approach is implemented in several R packages such as\nrobustbase::mc\nwhich is used in this implementation of\ndataquieR\n.\nAnother completely heuristic approach is also included to identify outliers.\nThe approach is based on the assumption that the distances between\nmeasurements of the same underlying distribution should homogeneous. For\ncomprehension of this approach:\nconsider an ordered sequence of all measurements.\nbetween these measurements all distances are calculated.\nthe occurrence of larger distances between two neighboring measurements\nmay\nthan indicate a distortion of the data. For the heuristic definition of a\nlarge distance\n1 * \\sigma\nhas been been chosen.\nNote, that the plots are not deterministic, because they use\nggplot2::geom_jitter\n.	resp_vars	f	study_data	meta_data	377004	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_acc_impl_univariate_outlier.html
2021-08-12 09:04:17.012232	300	acc_varcomp	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::acc_varcomp)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::acc_varcomp))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	t	\N	\N	0	2021-03-23 18:34:09.896893	Estimates variance components	Variance based models and intraclass correlations (ICC) are approaches to\nexamine the impact of so-called process variables on the measurements. This\nimplementation is model-based.\nNB:\nThe term ICC is frequently used to describe the agreement between\ndifferent observers, examiners or even devices. In respective settings a good\nagreement is pursued. ICC-values can vary between\n[-1;1]\nand an ICC close\nto 1 is desired (Koo and Li 2016, Müller and Büttner 1994).\nHowever, in multi-level analysis the ICC is interpreted differently. Please\nsee Snijders et al. (Sniders and Bosker 1999). In this context the proportion\nof variance explained by respective group levels indicate an influence of (at\nleast one) level of the respective group_vars. An ICC close to 0 is desired.	resp_vars	f	study_data	meta_data	377005	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_acc_impl_varcomp.html
2021-08-12 09:04:17.514328	302	com_segment_missingness	\N	\n      # from dataquieR (Version 1.0.6.9013), created by functionregistrator (Version 0.1.7)\n      .sc <- sys.call()\n      .sc[[1]] <- quote(dataquieR::com_segment_missingness)\n      .sc$label_col <- dataquieR::VAR_NAMES\n      f_formalargs <- names(formals(dataquieR::com_segment_missingness))\n      if (!('...' %in% f_formalargs)) {\n        not_an_arg <- names(.sc)[!(names(.sc) %in% f_formalargs)]\n        .sc[not_an_arg] <- NULL\n      }\n      eval(.sc, parent.frame())\n    	\N	\N	f	\N	\N	0	2021-03-23 18:34:09.896893	Summarizes missingness for individuals in specific segments	This implementation can be applied in two use cases:\nparticipation in study segments is not recorded by respective variables,\ne.g. a participant's refusal to attend a specific examination is not\nrecorded.\nparticipation in study segments is recorded by respective\nvariables.\nUse case\n(1)\nwill be common in smaller studies. For the calculation of\nsegment missingness it is assumed that study variables are nested in\nrespective segments. This structure must be specified in the static metadata.\nThe R-function identifies all variables within each segment and returns TRUE\nif all variables within a segment are missing, otherwise FALSE.\nUse case\n(2)\nassumes a more complex structure of study data and meta data.\nThe study data comprise so-called intro-variables (either TRUE/FALSE or codes\nfor non-participation). The column KEY_STUDY_SEGMENT in the metadata is\nfilled by variable-IDs indicating for each variable the respective\nintro-variable. This structure has the benefit that subsequent calculation of\nitem missingness obtains correct denominators for the calculation of\nmissingness rates.	resp_vars	f	study_data	meta_data	377007	raw	t	https://dataquality.ship-med.uni-greifswald.de/VIN_com_impl_segment_missingness.html
\.


COPY square.t_functioninput (lastchange, pk, fk_function, input_name, default_value, ordering, description, usage, fk_type, type_restriction, refers_metadata) FROM stdin;
2021-08-12 09:04:15.363731	976	292	group_vars	null	10	group_vars: <variable list>  the name of the observer, device or\n reader variable\nSupported variable types: integer, string, datetime\nThis argument expects variable names\nYou may enter more than one value	\N	15	{"vector": true, "read_only": [false], "variableDataTypes": ["integer", "string", "datetime"]}	f
2021-08-12 09:04:16.004755	984	295	group_vars	null	10	group_vars: <variable list>  len=1-1. the name of the observer, device or\n reader variable\nSupported variable types: integer, string, datetime\nThis argument expects variable names\nYou may not enter more than one value	\N	15	{"len": {"to": 1, "from": 1}, "vector": false, "read_only": [false], "variableDataTypes": ["integer", "string", "datetime"]}	f
2021-08-12 09:04:16.044575	985	295	co_vars	null	20	co_vars: <variable list>  a vector of covariables, e.g. age and sex for\n adjustment\nSupported variable types: integer, string, float, datetime\nThis argument expects variable names\nYou may enter more than one value	\N	15	{"vector": true, "read_only": [false], "variableDataTypes": ["integer", "string", "float", "datetime"]}	f
2021-08-12 09:04:16.064025	987	295	threshold_value	\N	40	threshold_value: <numeric>  a multiplier or absolute value see\n Implementation and use of thresholds\nYou may not enter more than one value	\N	1	{"read_only": [false]}	f
2021-08-12 09:04:16.073823	988	295	min_obs_in_subgroup	\N	50	min_obs_in_subgroup: <integer>  from=0. optional argument if a\n "group_var" is used.\n This argument specifies the\n minimum no. of observations that is\n required to include a subgroup (level)\n of the "group_var" in\n the analysis. Subgroups with less\n observations are excluded. The\n default is 5.\nYou may not enter more than one value	\N	16	{"from": 0, "read_only": [false]}	f
2021-08-12 09:04:16.273195	989	296	id_vars	null	10	id_vars: <variable>  optional, an ID variable of\n the study data. If not specified row numbers are used.\nSupported variable types: integer\nThis argument expects variable names\nYou may not enter more than one value	\N	15	{"vector": false, "read_only": [false], "variableDataTypes": ["integer"]}	f
2021-08-12 09:04:16.283134	990	296	n_rules	4	20	n_rules: <numeric>  from=1 to=4. the no. of rules that must be violated\n to classify as outlier\nYou may not enter more than one value	\N	1	{"to": 4, "from": 1, "read_only": [false]}	f
2021-08-12 09:04:16.481425	991	297	exclude_roles	\N	10	exclude_roles: <variable roles>  a character (vector) of variable roles\n not included\nYou may not enter more than one value	\N	23	{"vector": [true], "read_only": [false]}	f
2021-08-12 09:04:16.491314	992	297	n_rules	4	20	n_rules: <integer>  from=1 to=4. the no. of rules that must be violated\n to flag a variable as containing outliers. The default is 4, i.e. all.\nYou may not enter more than one value	\N	16	{"to": 4, "from": 1, "read_only": [false]}	f
2021-08-12 09:04:16.502002	993	297	max_non_outliers_plot	10000	30	max_non_outliers_plot: <integer>  from=0. Maximum number of non-outlier\n points to be plot. If more\n points exist, a subsample will\n be plotted only. Note, that\n sampling is not deterministic.\nYou may not enter more than one value	\N	16	{"from": 0, "read_only": [false]}	f
2021-08-12 09:04:16.713677	994	298	dist_col	\N	10	dist_col: <variable attribute>  the name of the variable attribute in\n meta_data that provides the expected\n distribution of a study variable\nYou may not enter more than one value	\N	20	{"read_only": [false]}	f
2021-08-12 09:04:16.724188	995	298	guess	\N	20	guess: <logical>  estimate parameters\nYou may not enter more than one value	\N	25	{"read_only": [false]}	f
2021-08-12 09:04:16.734042	996	298	par1	\N	30	par1: <numeric>  first parameter of the distribution if applicable\nYou may not enter more than one value	\N	1	{"read_only": [false]}	f
2021-08-12 09:04:16.754032	998	298	end_digits	\N	50	end_digits: <logical>  internal use. check for end digits preferences\nYou may not enter more than one value	\N	25	{"read_only": [true]}	f
2021-08-12 09:04:16.958094	999	299	exclude_roles	\N	10	exclude_roles: <variable roles>  a character (vector) of variable roles\n not included\nYou may not enter more than one value	\N	23	{"vector": [true], "read_only": [false]}	f
2021-08-12 09:04:16.978792	1001	299	max_non_outliers_plot	10000	30	max_non_outliers_plot: <integer>  from=0. Maximum number of non-outlier\n points to be plot. If more\n points exist, a subsample will\n be plotted only. Note, that\n sampling is not deterministic.\nYou may not enter more than one value	\N	16	{"from": 0, "read_only": [false]}	f
2021-08-12 09:04:17.163728	1002	300	group_vars	\N	10	group_vars: <variable list>  the names of the resp. observer, device or\n reader variables\nSupported variable types: integer, string, datetime\nThis argument expects variable names\nYou may not enter more than one value	\N	15	{"vector": false, "read_only": [false], "variableDataTypes": ["integer", "string", "datetime"]}	f
2021-08-12 09:04:17.175396	1003	300	co_vars	null	20	co_vars: <variable list>  a vector of covariables, e.g. age and sex for\n adjustment\nSupported variable types: integer, string, float, datetime\nThis argument expects variable names\nYou may enter more than one value	\N	15	{"vector": true, "read_only": [false], "variableDataTypes": ["integer", "string", "float", "datetime"]}	f
2021-08-12 09:04:17.185868	1004	300	min_obs_in_subgroup	30	30	min_obs_in_subgroup: <integer>  from=0. optional argument if a\n "group_var" is used. This argument\n specifies the minimum no. of observations\n that is required to include a subgroup\n (level) of the "group_var" in the analysis.\n Subgroups with less observations are\n excluded. The default is 30.\nYou may not enter more than one value	\N	16	{"from": 0, "read_only": [false]}	f
2021-08-12 09:04:17.200692	1005	300	min_subgroups	5	40	min_subgroups: <integer>  from=0. optional argument if a "group_var" is\n used. This argument specifies the\n minimum no. of subgroups (levels)\n included "group_var". If the variable\n defined in "group_var" has less\n subgroups it is not used for analysis.\n The default is 5.\nYou may not enter more than one value	\N	16	{"from": 0, "read_only": [false]}	f
2021-08-12 09:04:17.211654	1006	300	threshold_value	0.05	50	threshold_value: <numeric>  from=0 to=1. a numerical value ranging\n from 0-1\nYou may not enter more than one value	\N	1	{"to": 1, "from": 0, "read_only": [false]}	f
2021-08-12 09:04:17.416995	1007	301	show_causes	true	10	show_causes: <logical>  if TRUE, then the distribution of missing codes\n is shown\nYou may not enter more than one value	\N	25	{"read_only": [false]}	f
2021-08-12 09:04:17.445348	1008	301	cause_label_df	\N	20	cause_label_df: <data.frame>  missing code table. If missing codes have\n labels the respective data frame must be\n specified here\nYou may not enter more than one value	\N	22	{"content": ["missing code table"], "read_only": [false]}	f
2021-08-12 09:04:17.45599	1009	301	include_sysmiss	null	30	include_sysmiss: <logical>  Optional, if TRUE system missingness (NAs)\n is evaluated in the summary plot\nYou may not enter more than one value	\N	25	{"read_only": [false]}	f
2021-08-12 09:04:17.465805	1010	301	threshold_value	\N	40	threshold_value: <numeric>  from=0 to=100. a numerical value ranging\n from 0-100\nYou may not enter more than one value	\N	1	{"to": 100, "from": 0, "read_only": [false]}	f
2021-08-12 09:04:17.475047	1011	301	suppressWarnings	false	50	suppressWarnings: <logical>  warn about mixed missing and jump code\n lists\nYou may not enter more than one value	\N	25	{"read_only": [false]}	f
2021-08-12 09:04:17.659266	1012	302	group_vars	null	10	group_vars: <variable>  the name of a variable used for grouping,\n defaults to  NULL  for not grouping output\nSupported variable types: integer, string, datetime\nThis argument expects variable names\nYou may not enter more than one value	\N	15	{"vector": false, "read_only": [false], "variableDataTypes": ["integer", "string", "datetime"]}	f
2021-08-12 09:04:17.671195	1013	302	strata_vars	null	20	strata_vars: <variable>  the name of a variable used for stratification,\n defaults to NULL for not grouping output\nSupported variable types: integer, string, datetime\nThis argument expects variable names\nYou may not enter more than one value	\N	15	{"vector": false, "read_only": [false], "variableDataTypes": ["integer", "string", "datetime"]}	f
2021-08-12 09:04:17.681671	1014	302	threshold_value	\N	30	threshold_value: <numeric>  from=0 to=100. a numerical value ranging\n from 0-100\nYou may not enter more than one value	\N	1	{"to": 100, "from": 0, "read_only": [false]}	f
2021-08-12 09:04:17.691299	1015	302	direction	\N	40	direction: <enum>  low | high. "high" or "low", i.e. are deviations\n above/below the threshold critical\nYou may not enter more than one value	\N	19	{"levels": ["low", "high"], "read_only": [false]}	f
2021-08-12 09:04:17.701411	1016	302	exclude_roles	"process"	50	exclude_roles: <variable roles>  a character (vector) of variable roles\n not included\nYou may not enter more than one value	\N	23	{"vector": [true], "read_only": [false]}	f
2021-08-12 09:04:17.886931	1017	303	id_vars	null	10	id_vars: <variable list>  optional, a (vectorized) call of ID-variables\n that should not be\n considered in the calculation of unit-\n missingness\nSupported variable types: integer, string, float, datetime\nThis argument expects variable names\nYou may enter more than one value	\N	15	{"vector": true, "read_only": [false], "variableDataTypes": ["integer", "string", "float", "datetime"]}	f
2021-08-12 09:04:17.898739	1018	303	strata_vars	null	20	strata_vars: <variable>  optional, a string or integer variable used for\n stratification\nSupported variable types: integer, string, datetime\nThis argument expects variable names\nYou may not enter more than one value	\N	15	{"vector": false, "read_only": [false], "variableDataTypes": ["integer", "string", "datetime"]}	f
2021-08-12 09:04:18.086563	1019	304	threshold_value	\N	10	threshold_value: <numeric>  from=0 to=100. a numerical value\n ranging from 0-100\nYou may not enter more than one value	\N	1	{"to": 100, "from": 0, "read_only": [false]}	f
2021-08-12 09:04:18.096906	1020	304	check_table	\N	20	check_table: <data.frame>  contradiction rules table.  Table defining\n contractions. See details for\n its required structure.\nYou may not enter more than one value	\N	22	{"content": ["contradiction rules table"], "read_only": [false]}	f
2021-08-12 09:04:16.054535	986	295	threshold_type	null	30	threshold_type: <enum>  empirical | user | none. In case empirical is\n chosen a multiplier of the scale measure is used,\n in case of user a value of the mean or probability\n (binary data) has to be defined see Implementation\n and use of thresholds. In case of none, no thresholds\n are displayed and no flagging of\n unusual group levels is applied.\nYou may not enter more than one value	\N	19	{"levels": ["empirical", "user", "none"], "read_only": [false]}	f
2021-08-12 09:04:16.744189	997	298	par2	\N	40	par2: <numeric>  second parameter of the distribution if applicable\nYou may not enter more than one value	\N	1	{"read_only": [false]}	f
2021-08-12 09:04:16.968655	1000	299	n_rules	4	20	n_rules: <integer>  from=1 to=4. the no. of rules that must be violated\n to flag a variable as containing outliers. The default is 4, i.e. all.\nYou may not enter more than one value	\N	16	{"to": 4, "from": 1, "read_only": [false]}	f
2021-08-12 09:04:18.106873	1021	304	summarize_categories	false	30	summarize_categories: <logical>  Needs a column 'tag' in the\n check_table .\n If set, a summary output is generated for the\n defined categories plus one plot per\n category.\nYou may not enter more than one value	\N	25	{"read_only": [false]}	f
2021-08-12 09:04:18.318169	1022	305	limits	\N	10	limits: <enum>  HARD_LIMITS | SOFT_LIMITS | DETECTION_LIMITS. what\n limits from metadata to check for\nYou may not enter more than one value	\N	19	{"levels": ["HARD_LIMITS", "SOFT_LIMITS", "DETECTION_LIMITS"], "read_only": [false]}	f
2021-08-12 09:04:18.490953	1023	306	threshold	null	10	threshold: <numeric>  from=0 to=100. a numerical value ranging\n from 0-100. Not yet implemented.\nYou may not enter more than one value	\N	1	{"to": 100, "from": 0, "read_only": [false]}	f
2021-08-12 09:04:18.698809	1024	307	limits	\N	10	limits: <enum>  HARD_LIMITS | SOFT_LIMITS | DETECTION_LIMITS. what\n limits from metadata to check for\nYou may not enter more than one value	\N	19	{"levels": ["HARD_LIMITS", "SOFT_LIMITS", "DETECTION_LIMITS"], "read_only": [false]}	f
2021-08-12 09:04:18.934213	1026	311	split_segments	false	10	split_segments: <logical>  return one matrix per study segment\nYou may not enter more than one value	\N	25	{"read_only": [false]}	f
2021-08-12 09:04:18.946862	1027	311	max_vars_per_plot	20	20	max_vars_per_plot: <integer>  from=0. The maximum number of variables\n per single plot.\nYou may not enter more than one value	\N	16	{"from": 0, "read_only": [false]}	f
2021-08-12 09:04:18.958409	1028	311	threshold_value	0	30	threshold_value: <numeric>  from=0 to=100. percentage failing\n conversions allowed to still classify a\n study variable convertible.\nYou may not enter more than one value	\N	1	{"to": 100, "from": 0, "read_only": [false]}	f
\.


COPY square.t_functionoutput (lastchange, pk, fk_function, name, description, type) FROM stdin;
2021-08-12 09:04:18.133626	1337	304	SummaryData	SummaryData	0
2021-08-12 09:04:18.142243	1338	304	SummaryPlot	SummaryPlot	0
2021-08-12 09:04:18.500257	1339	306	SummaryTable	SummaryTable	0
2021-08-12 09:04:18.50884	1340	306	ModifiedStudyData	ModifiedStudyData	0
2021-08-12 09:04:18.517516	1341	306	FlaggedStudyData	FlaggedStudyData	0
2021-08-12 09:04:18.708847	1342	307	FlaggedStudyData	FlaggedStudyData	0
2021-08-12 09:04:18.718204	1343	307	SummaryTable	SummaryTable	0
2021-08-12 09:04:18.727417	1344	307	SummaryPlotList	SummaryPlotList	0
2021-08-12 09:04:18.736748	1345	307	ModifiedStudyData	ModifiedStudyData	0
2021-08-12 09:04:18.969021	1349	311	SummaryPlot	SummaryPlot	0
2021-08-12 09:04:15.380695	1311	292	SummaryPlotList	SummaryPlotList	0
2021-08-12 09:04:15.560391	1312	293	SummaryData	SummaryData	0
2021-08-12 09:04:15.569632	1313	293	SummaryPlot	SummaryPlot	0
2021-08-12 09:04:16.082051	1315	295	SummaryData	SummaryData	0
2021-08-12 09:04:16.090626	1316	295	SummaryTable	SummaryTable	0
2021-08-12 09:04:16.099205	1317	295	SummaryPlot	SummaryPlot	0
2021-08-12 09:04:16.29321	1318	296	FlaggedStudyData	FlaggedStudyData	0
2021-08-12 09:04:16.301823	1319	296	SummaryTable	SummaryTable	0
2021-08-12 09:04:16.309894	1320	296	SummaryPlot	SummaryPlot	0
2021-08-12 09:04:16.511222	1321	297	SummaryTable	SummaryTable	0
2021-08-12 09:04:16.51955	1322	297	SummaryPlotList	SummaryPlotList	0
2021-08-12 09:04:16.763158	1323	298	SummaryData	SummaryData	0
2021-08-12 09:04:16.771837	1324	298	SummaryPlot	SummaryPlot	0
2021-08-12 09:04:16.780935	1325	298	SummaryTable	SummaryTable	0
2021-08-12 09:04:16.987668	1326	299	SummaryTable	SummaryTable	0
2021-08-12 09:04:16.995987	1327	299	SummaryPlotList	SummaryPlotList	0
2021-08-12 09:04:17.220423	1328	300	SummaryTable	SummaryTable	0
2021-08-12 09:04:17.229167	1329	300	ScalarValue_max_icc	ScalarValue_max_icc	0
2021-08-12 09:04:17.237915	1330	300	ScalarValue_argmax_icc	ScalarValue_argmax_icc	0
2021-08-12 09:04:18.978213	1350	311	DataTypePlotList	DataTypePlotList	0
2021-08-12 09:04:18.98835	1351	311	SummaryTable	SummaryTable	0
2021-08-12 09:04:19.175342	1346	308	rdata	rdata	0
2021-08-12 09:04:17.483671	1331	301	SummaryTable	SummaryTable	0
2021-08-12 09:04:17.492543	1332	301	SummaryPlot	SummaryPlot	0
2021-08-12 09:04:17.710568	1333	302	SummaryData	SummaryData	0
2021-08-12 09:04:17.719275	1334	302	SummaryPlot	SummaryPlot	0
2021-08-12 09:04:18.11579	1335	304	FlaggedStudyData	FlaggedStudyData	0
2021-08-12 09:04:18.124714	1336	304	SummaryTable	SummaryTable	0
2021-08-12 09:04:19.360197	1347	309	rdata	rdata	0
2021-08-12 09:04:19.544822	1348	310	rdata	rdata	0
\.


COPY square.t_functioncategory (lastchange, pk, fk_function, name, description) FROM stdin;
2021-03-23 18:34:09.896893	243	292	Accuracy	\N
2021-03-23 18:34:09.896893	244	293	Accuracy	\N
2021-03-23 18:34:09.896893	246	295	Accuracy	\N
2021-03-23 18:34:09.896893	247	296	Accuracy	\N
2021-03-23 18:34:09.896893	248	297	Accuracy	\N
2021-03-23 18:34:09.896893	249	298	Accuracy	\N
2021-03-23 18:34:09.896893	250	299	Accuracy	\N
2021-03-23 18:34:09.896893	251	300	Accuracy	\N
2021-03-23 18:34:09.896893	252	301	Completeness	\N
2021-03-23 18:34:09.896893	253	302	Completeness	\N
2021-03-23 18:34:09.896893	254	303	Completeness	\N
2021-03-23 18:34:09.896893	255	304	Consistency	\N
2021-03-23 18:34:09.896893	256	305	Consistency	\N
2021-03-23 18:34:09.896893	257	306	Consistency	\N
2021-03-23 18:34:09.896893	258	307	Consistency	\N
2021-03-23 18:34:09.896893	259	308	Debug	\N
2021-03-23 18:34:09.896893	260	309	Debug	\N
2021-03-23 18:34:09.896893	261	310	Debug	\N
2021-06-04 17:28:33.751699	300	\N	Integrity	DQ Dimension Integrity
2021-06-04 17:28:33.751699	317	311	Integrity	\N
\.

COPY square.t_functionscale (lastchange, pk, fk_function, fk_scale) FROM stdin;
2021-03-23 18:34:09.896893	3173	292	6
2021-03-23 18:34:09.896893	3174	292	2
2021-03-23 18:34:09.896893	3175	292	4
2021-03-23 18:34:09.896893	3176	293	6
2021-03-23 18:34:09.896893	3177	293	2
2021-03-23 18:34:09.896893	3178	293	4
2021-03-23 18:34:09.896893	3186	295	6
2021-03-23 18:34:09.896893	3187	295	2
2021-03-23 18:34:09.896893	3188	295	4
2021-03-23 18:34:09.896893	3189	296	6
2021-03-23 18:34:09.896893	3190	296	2
2021-03-23 18:34:09.896893	3191	296	4
2021-03-23 18:34:09.896893	3192	297	6
2021-03-23 18:34:09.896893	3193	297	2
2021-03-23 18:34:09.896893	3194	297	4
2021-03-23 18:34:09.896893	3195	298	6
2021-03-23 18:34:09.896893	3196	298	2
2021-03-23 18:34:09.896893	3197	298	5
2021-03-23 18:34:09.896893	3198	298	1
2021-03-23 18:34:09.896893	3199	298	3
2021-03-23 18:34:09.896893	3200	298	4
2021-03-23 18:34:09.896893	3201	298	7
2021-03-23 18:34:09.896893	3202	299	6
2021-03-23 18:34:09.896893	3203	299	2
2021-03-23 18:34:09.896893	3204	299	4
2021-03-23 18:34:09.896893	3205	300	6
2021-03-23 18:34:09.896893	3206	300	2
2021-03-23 18:34:09.896893	3207	300	4
2021-03-23 18:34:09.896893	3208	301	6
2021-03-23 18:34:09.896893	3209	301	2
2021-03-23 18:34:09.896893	3210	301	5
2021-03-23 18:34:09.896893	3211	301	1
2021-03-23 18:34:09.896893	3212	301	3
2021-03-23 18:34:09.896893	3213	301	4
2021-03-23 18:34:09.896893	3214	301	7
2021-03-23 18:34:09.896893	3215	302	7
2021-03-23 18:34:09.896893	3216	303	7
2021-03-23 18:34:09.896893	3217	304	6
2021-03-23 18:34:09.896893	3218	304	2
2021-03-23 18:34:09.896893	3219	304	5
2021-03-23 18:34:09.896893	3220	304	1
2021-03-23 18:34:09.896893	3221	304	3
2021-03-23 18:34:09.896893	3222	304	4
2021-03-23 18:34:09.896893	3223	304	7
2021-03-23 18:34:09.896893	3224	305	7
2021-03-23 18:34:09.896893	3225	306	6
2021-03-23 18:34:09.896893	3226	306	2
2021-03-23 18:34:09.896893	3227	306	5
2021-03-23 18:34:09.896893	3228	306	1
2021-03-23 18:34:09.896893	3229	306	3
2021-03-23 18:34:09.896893	3230	307	6
2021-03-23 18:34:09.896893	3231	307	2
2021-03-23 18:34:09.896893	3232	307	4
2021-03-23 18:34:09.896893	3233	307	7
2021-03-23 18:34:09.896893	3234	308	6
2021-03-23 18:34:09.896893	3235	308	2
2021-03-23 18:34:09.896893	3236	308	5
2021-03-23 18:34:09.896893	3237	308	1
2021-03-23 18:34:09.896893	3238	308	3
2021-03-23 18:34:09.896893	3239	308	4
2021-03-23 18:34:09.896893	3240	308	7
2021-03-23 18:34:09.896893	3241	309	6
2021-03-23 18:34:09.896893	3242	309	2
2021-03-23 18:34:09.896893	3243	309	5
2021-03-23 18:34:09.896893	3244	309	1
2021-03-23 18:34:09.896893	3245	309	3
2021-03-23 18:34:09.896893	3246	309	4
2021-03-23 18:34:09.896893	3247	309	7
2021-03-23 18:34:09.896893	3248	310	6
2021-03-23 18:34:09.896893	3249	310	2
2021-03-23 18:34:09.896893	3250	310	5
2021-03-23 18:34:09.896893	3251	310	1
2021-03-23 18:34:09.896893	3252	310	3
2021-03-23 18:34:09.896893	3253	310	4
2021-03-23 18:34:09.896893	3254	310	7
2021-06-04 17:28:33.751699	3480	311	6
2021-06-04 17:28:33.751699	3481	311	2
2021-06-04 17:28:33.751699	3482	311	5
2021-06-04 17:28:33.751699	3483	311	1
2021-06-04 17:28:33.751699	3484	311	3
2021-06-04 17:28:33.751699	3485	311	4
2021-06-04 17:28:33.751699	3486	311	7
\.


select reset_sequences();
