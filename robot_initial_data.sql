COPY square.t_person (lastchange, usnr, username, forename, surname, contact) FROM stdin;
2022-12-01 13:57:59.155063	1	testperson	Testimonie	Testa	\N
2022-12-01 13:59:37.228731	2	blanktest	Robert	Rost	\N
\.

COPY square.t_login (lastchange, fk_usnr, password, expire_on) FROM stdin;
2022-12-15 16:54:39.329265	2	PAFDTSAjbqv12n4Prx1asQiPwlRlz6UQvshRxvViKURxE6vw+gSfCkOHd3mTe7xj	2037-12-18 00:00:00
2022-12-15 16:55:20.487366	1	0o3Cp3WDBoZYRjYN2GXe/0S/BcDx8SAvX8OuwQlnQjaclkWpG2yYLgHKvzCzXCcN	2037-12-18 00:00:00
\.

COPY square.t_privilege (lastchange, pk, name) FROM stdin;
2022-12-01 14:04:29.536532	2	\N
2022-12-01 14:04:29.536532	3	parent.of.privilege.1
2022-12-01 14:04:29.536532	1	element.2
2022-12-01 14:05:14.120414	5	\N
2022-12-01 14:05:14.120414	4	element.3
2022-12-01 14:05:52.713016	6	element.4
2022-12-01 14:06:59.461194	7	element.5
2022-12-01 14:07:48.915779	8	variablegroup.1
\.

COPY square.t_userprivilege (lastchange, pk, fk_privilege, fk_usnr, square_acl) FROM stdin;
2022-12-01 13:56:17.099998	1174832	-314	0	rwx
2022-12-01 13:56:26.506914	1174796	-313	0	rwx
2022-12-01 13:56:31.627228	1174797	-312	0	rwx
2022-12-01 13:56:36.694931	1174798	-311	0	rwx
2022-12-01 13:56:41.95053	1174799	-310	0	rwx
2022-12-01 13:56:47.308825	1174800	-309	0	rwx
2022-12-01 13:56:53.319642	1174801	-308	0	rwx
2022-12-01 13:56:59.581336	1174802	-307	0	rwx
2022-12-01 13:57:05.827387	1174803	-306	0	rwx
2022-12-01 13:57:11.797856	1174804	-305	0	rwx
2022-12-01 13:57:17.481875	1174805	-304	0	rwx
2022-12-01 13:57:24.4594	1174806	-303	0	rwx
2022-12-01 13:57:30.903722	1174807	-302	0	rwx
2022-12-01 13:57:36.619516	1174808	-301	0	rwx
2022-12-01 14:01:02.268185	1174809	-314	1	rwx
2022-12-01 14:01:07.847766	1174810	-313	1	rwx
2022-12-01 14:01:12.937142	1174811	-312	1	rwx
2022-12-01 14:01:17.750172	1174812	-311	1	rwx
2022-12-01 14:01:22.238729	1174813	-310	1	rwx
2022-12-01 14:01:28.333801	1174814	-309	1	rwx
2022-12-01 14:01:33.264596	1174815	-308	1	rwx
2022-12-01 14:01:39.512458	1174816	-307	1	rwx
2022-12-01 14:01:46.264062	1174817	-306	1	rwx
2022-12-01 14:01:52.671187	1174818	-305	1	rwx
2022-12-01 14:01:59.509218	1174819	-304	1	rwx
2022-12-01 14:02:05.26111	1174820	-303	1	rwx
2022-12-01 14:02:15.136665	1174821	-302	1	rwx
2022-12-01 14:02:21.193278	1174822	-301	1	rwx
2022-12-01 14:02:29.019905	1174823	-300	1	rwx
2022-12-01 14:04:29.536532	1174824	1	0	rwx
2022-12-01 14:04:29.536532	1174825	2	0	rwx
2022-12-01 14:05:14.120414	1174826	4	0	rwx
2022-12-01 14:05:14.120414	1174827	5	0	rwx
2022-12-01 14:04:29.536532	1174828	3	0	rwx
2022-12-01 14:05:52.713016	1174829	6	0	rwx
2022-12-01 14:06:59.461194	1174830	7	0	rwx
2022-12-01 14:07:48.915779	1174831	8	0	rwx
\.


COPY square.t_element (lastchange, pk, fk_parent, fk_lang, translation, fk_privilege, name, order_nr, unique_name, fk_missinglist) FROM stdin;
2022-12-01 14:04:29.536532	2	1	\N	{}	1	ROBOT-0	\N	robot0	\N
2022-12-01 14:05:14.120414	3	1	\N	{"en": "Test", "de_DE": "Testung"}	4	ROBOT-1	\N	robot1	\N
2022-12-01 14:05:52.713016	4	3	\N	{"en": "For test purposes", "de_DE": "Zum Zwecke der Testungen"}	6	rr_department1	\N	robot1.rrdepartment1	\N
2022-12-01 14:06:03.51395	1	\N	\N	{}	3	ROBOT	\N	ROBOT	\N
2022-12-01 14:07:24.683098	5	4	\N	{"en": "for test purposes", "de_DE": "zum testungs-zweckeäüö"}	7	rr_variable1	\N	robot1.rrvariable1	\N
\.


COPY square.t_studygroup (lastchange, pk, fk_element, locales) FROM stdin;
2022-12-01 14:03:29.008583	1	1	{"en": "English", "de_DE": "Deutsch"}
\.

COPY square.t_study (lastchange, pk, fk_element, start_date, designfeature, participants) FROM stdin;
2022-12-01 14:04:29.536532	1	2	2022-12-23 00:00:00	2022-10-26 00:00:00	100
2022-12-01 14:05:14.120414	2	3	1857-01-01 00:00:00	2020-11-16 00:00:00	1000
\.


COPY square.t_variablegroup (lastchange, pk, name, description, fk_privilege) FROM stdin;
2022-12-01 14:07:48.915779	1	rr_variableselection	Test test test	8
\.



COPY square.t_variable (lastchange, fk_element, column_name, fk_scale, var_order, optional, control_type, valuelist, fk_variableusage, datatype, idvariable) FROM stdin;
2022-12-01 14:06:59.461194	5	variable1	\N	\N	f	\N		\N	integer	t
\.


COPY square.t_variablegroupelement (lastchange, pk, fk_variablegroup, fk_element, varorder) FROM stdin;
2022-12-01 14:21:09.309071	3	1	5	5
\.

SELECT reset_sequences();
