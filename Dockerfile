FROM postgres:14
RUN apt-get update && apt-get install -y \
    git \
    postgresql-14-plr \
 && rm -rf /var/lib/apt/lists/*

COPY postgresql.conf /etc/postgresql/postgresql.conf
COPY robot_initial_data.sql /docker-entrypoint-initdb.d/X_robot.sql
COPY t_synthetic.sql /docker-entrypoint_initdb.d/Y_synthetic_data.sql
COPY r_functions.sql /docker-entrypoint-initdb.d/Z_functions.sql
COPY start.sh /bin/

ADD https://gitlab.com/libreumg/square2webapp/database/squaredb/-/commits/repair/ /tmp/
ADD https://gitlab.com/libreumg/square2webapp/container/square2dbimg/-/commits/dbupdate/ /tmp/
RUN chmod +x /bin/start.sh \
 && git clone -b dbupdate https://gitlab.com/libreumg/square2webapp/container/square2dbimg.git \
 && git clone --branch repair https://gitlab.com/libreumg/square2webapp/database/squaredb.git \
 && mv squaredb/* / \
 && mv square2dbimg/db.sql/* /docker-entrypoint-initdb.d/ \
 && mkdir /etc/postgresql/log && chown postgres /etc/postgresql/log \
 && rm -rf square2dbimg \
 && rm -rf squaredb 

EXPOSE 5432
ENTRYPOINT ["/bin/start.sh"]
CMD ["-c", "config_file=/etc/postgresql/postgresql.conf"]
